#include <stdio.h>
#include <stdlib.h>
#include <ruby.h>

// Load a file into a buffer
// allocates memory and returns a pointer to it
// 
unsigned char *load_file(const char *name,uint64_t *size)
{
FILE    *fp;
unsigned int len;
unsigned char *buffer;

  //  Try to open the file 
  fp = fopen(name,"rb");
  //  if we can't open the file Error out
  if (fp==NULL)
  {
    printf("ERR:Opening %s\n",name);
    exit(-1);
  }
  //  get the length, seek to the end, ask where we are, then seek back to beginning
  fseek(fp,0,SEEK_END);
  len = ftell(fp);
  fseek(fp,0,SEEK_SET);

  //  allocate the buffer
  buffer = (unsigned char*)malloc(len+1);
  if (buffer == NULL)
  {
      printf("ERR:Allocating %x bytes\n",len);
      exit(-1);
  }
  //  extra step ensure the buffer is NULL terminated
  buffer[len] = 0;
  printf("reading %s int buffer of size %d bytes\n",name,len);
  
  fread(buffer,len,1,fp);
  return (buffer);
}

// call a ruby function or source file 
// print error message
void rubyEval(const char *func)
{
  int state;  
  VALUE result;
  result = rb_eval_string_protect(func, &state);
  if (state)
  {
    VALUE errorLineNumber = rb_gv_get("$errorLineNumber");    
    VALUE errorString = rb_inspect(rb_gv_get("$!"));
    printf( "ruby eval failed : %d\n", state);
    printf( "ruby $! : %s", StringValueCStr(errorString));
    exit(0);
  }
}

// call a ruby function without error checking
// for example if you don't care if the function doesn't exist
void rubyFuncIgnoreFail(const char *func)
{
  int state;  
  VALUE result;
  result = rb_eval_string_protect(func, &state);
}

// fictional print
// shows how to parse a string and two numbers
static VALUE rb_Print(VALUE self,VALUE str,VALUE x,VALUE y)
{
  //  as floats
  printf("[%s] at %f,%f\n",StringValueCStr(str),NUM2DBL(x),NUM2DBL(y));
  //  as ints
  printf("[%s] at %d,%d\n",StringValueCStr(str),NUM2INT(x),NUM2INT(y));
  return Qnil;
}

//  fictional mouse function 
//  shows how to return multiple values
static VALUE rb_Mouse(VALUE self)
{
  int x = 32;
  int y = 12;
  int but = 1;
	return rb_ary_new_from_args(3, INT2NUM(x), INT2NUM(y),INT2NUM(but));
}

//  require function
//  loads an additional source file and evaluates it
static VALUE rb_Require(VALUE self,VALUE str)
{
char fstring[256];
unsigned char *source;
  sprintf(fstring,"%s.rb\0",StringValueCStr(str));
  source = load_file(fstring,NULL);
  if (source!=NULL)
    rubyEval(source);
  return Qnil;
}

void rubyInit(const char *fname) 
{
int x;  
  printf("Ruby\n");
	ruby_init();
  printf("Ruby LoadPath\n");
	ruby_init_loadpath();

  //  define custom functions
  rb_define_method(rb_mKernel, "require",rb_Require, 1);  //  require takes 1 argument
  rb_define_method(rb_mKernel, "print",rb_Print, 3);      //  print takes 3 arguments
  rb_define_method(rb_mKernel, "mouse",rb_Mouse, 0);      //  mouse requires 0 arguments
  //  define constants
  rb_define_global_const("DEMOCONSTANT",INT2NUM(12));

  //  get ruby ready to run 
  printf("ruby_script\n");
  ruby_script("main");
  //  load our file and evaluate it
  printf("load & eval file\n");
  rubyEval(load_file(fname,NULL));
  
  //  we want to call a function ( say every update )
  rubyEval("demofunc");
}

int main(int argc,char *argv)
{
  rubyInit("demo.rb");
}
